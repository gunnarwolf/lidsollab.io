---
title: Diego Barriga
name: Diego Barriga
authors:
- umoqnier

superuser: false

role: Tesorero del Capítulo Estudiantil de la ACM UNAM-FI

bio: >
  Pasante de ingeniería en computación con amplio interés en NLP
  aplicado a lenguas mexicanas y en general a lenguas de bajos recursos
  digitales. Apasionado por la cultura libre y el software libre.

education:
  courses:
  - course: Ingeniería en Computación
    institution: Facultad de Ingeniería, UNAM
  - course: Introducción a la Critografía 
    institution: LIDSOL, UNAM
  - course: Curso para Spring Master 
    institution: Dev.f
  - course: Backend con Django
    institution: Dev.f
  - course: Programa de Becarios de la Unidad de Cómputo Académico
    institution: Facultad de Ingeniería, UNAM

social:
- icon: envelope
  icon_pack: fas
  link: "http://scr.im/mazacuato"
- icon: twitter
  icon_pack: fab
  link: "https://twitter.com/umoqnier"
- icon: github
  icon_pack: fab
  link: "https://github.com/umoqnier"
- icon: gitlab
  icon_pack: fab
  link: "https://gitlab.com/umoqnier"


user_groups:
  - Alumnos
---

Me gusta programar en *None*, promuevo la privacidad y el anonimato,
hago *&lt;undefined&gt;* y soy músico de closet.

