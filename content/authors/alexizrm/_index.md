---
title: Alexis Ríos
name: Alexis Ríos
authors:
- alexizrm

superuser: false

role:

bio: >
    Estudiante de ingeniería en computación, con intereses enfocados en el 
    desarrollo de aplicaciones móviles y seguridad informática

education:
  courses:
  - course: Ingeniería en Computación
    institution: Facultad de Ingeniería, UNAM

social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:soir_0907@hotmail.com'  # For a direct email link, use "mailto:test@example.org".

user_groups:
  - Alumnos
---

Estudiante de ingeniería en computación, con intereses enfocados en el 
desarrollo de aplicaciones móviles y seguridad informática
