---
name: Paul Aguilar (Æsahættr Penserbjorne)
title: Paul Aguilar (Æsahættr Penserbjorne)
authors:
- penserbjorne

superuser: false

organizations:
- name: Seguridad Digital en SocialTIC
  url: "https://socialtic.org/"
- name: Comunidad Elotl
  url: "https://www.facebook.com/comunidadelotl/"

bio: >
  Interesado en tecnologías libres, inteligencia artificial, seguridad
  informatica, hacktivismo y derechos humanos. Lenguajes de programación
  que salvarán al mundo: ASM, C/C++, Rust y LISP.

education:
  courses:
  - course: Ingeniería en Computación
    institution: Facultad de Ingeniería, UNAM
  - course: Matemáticas
    institution: Facultad de Ciencias, UNAM


social:
- icon: envelope
  icon_pack: fas
  link: "http://scr.im/47an"
- icon: globe
  icon_pack: fas
  link: "https://gwolf.org"
- icon: github
  icon_pack: fab
  link: "https://github.com/gwolf"

user_groups:
  - Alumnos
---

Tapatío de nacimiento, libre de corazón, perdido en la Ciudad
Monstruo. Maestro Jedi de la seguridad digital en SocialTIC de día,
disque bot hacktivista en Internet por las noches. Supuesto Ingeniero en
Computación, intento fallido de matemático. Causante del Tercer Impacto
con software libre en lenguas indígenas desde LIDSOL y Elotl. El futuro
esta escrito por hechiceros en LISP y Rust. No soy nadie, no soy nada.
Me gusta el tejuino y las jericallas :heart:
